/*******************************************************************************
The content of this file includes portions of the AUDIOKINETIC Wwise Technology
released in source code form as part of the SDK installer package.

Commercial License Usage

Licensees holding valid commercial licenses to the AUDIOKINETIC Wwise Technology
may use this file in accordance with the end user license agreement provided
with the software or, alternatively, in accordance with the terms contained in a
written agreement between you and Audiokinetic Inc.

Apache License Usage

Alternatively, this file may be used under the Apache License, Version 2.0 (the
"Apache License"); you may not use this file except in compliance with the
Apache License. You may obtain a copy of the Apache License at
http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed
under the Apache License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
OR CONDITIONS OF ANY KIND, either express or implied. See the Apache License for
the specific language governing permissions and limitations under the License.

  Copyright (c) 2022 Audiokinetic Inc.
*******************************************************************************/

#include "GlueCompressorFX.h"
#include "../GlueCompressorConfig.h"

#include <AK/AkWwiseSDKVersion.h>

AK::IAkPlugin* CreateGlueCompressorFX(AK::IAkPluginMemAlloc* in_pAllocator)
{
    return AK_PLUGIN_NEW(in_pAllocator, GlueCompressorFX());
}

AK::IAkPluginParam* CreateGlueCompressorFXParams(AK::IAkPluginMemAlloc* in_pAllocator)
{
    return AK_PLUGIN_NEW(in_pAllocator, GlueCompressorFXParams());
}

AK_IMPLEMENT_PLUGIN_FACTORY(GlueCompressorFX, AkPluginTypeEffect, GlueCompressorConfig::CompanyID, GlueCompressorConfig::PluginID)

GlueCompressorFX::GlueCompressorFX()
    : m_pParams(nullptr)
    , m_pAllocator(nullptr)
    , m_pContext(nullptr)
{
}

GlueCompressorFX::~GlueCompressorFX()
{
}

AKRESULT GlueCompressorFX::Init(AK::IAkPluginMemAlloc* in_pAllocator, AK::IAkEffectPluginContext* in_pContext, AK::IAkPluginParam* in_pParams, AkAudioFormat& in_rFormat)
{
    m_pParams = (GlueCompressorFXParams*)in_pParams;
    m_pAllocator = in_pAllocator;
    m_pContext = in_pContext;

    return AK_Success;
}

AKRESULT GlueCompressorFX::Term(AK::IAkPluginMemAlloc* in_pAllocator)
{
    AK_PLUGIN_DELETE(in_pAllocator, this);
    return AK_Success;
}

AKRESULT GlueCompressorFX::Reset()
{
    return AK_Success;
}

AKRESULT GlueCompressorFX::GetPluginInfo(AkPluginInfo& out_rPluginInfo)
{
    out_rPluginInfo.eType = AkPluginTypeEffect;
    out_rPluginInfo.bIsInPlace = true;
	out_rPluginInfo.bCanProcessObjects = true;
    out_rPluginInfo.uBuildVersion = AK_WWISESDK_VERSION_COMBINED;
    return AK_Success;
}

void GlueCompressorFX::Execute(const AkAudioObjects& io_objects)
{
	for (AkUInt32 objIdx = 0; objIdx < io_objects.uNumObjects; ++objIdx)
	{
		const AkUInt32 uNumChannels = io_objects.ppObjectBuffers[objIdx]->NumChannels();

		AkUInt16 uFramesProcessed;
		for (AkUInt32 i = 0; i < uNumChannels; ++i)
		{
			// Peek into object metadata if desired.
			AkAudioObject* pObject = io_objects.ppObjects[objIdx];
			
			AkReal32* AK_RESTRICT pBuf = (AkReal32* AK_RESTRICT)io_objects.ppObjectBuffers[objIdx]->GetChannel(i);

			uFramesProcessed = 0;
			while (uFramesProcessed < io_objects.ppObjectBuffers[objIdx]->uValidFrames)
			{
				// Execute DSP in-place here
				++uFramesProcessed;
			}
		}
	}
}
